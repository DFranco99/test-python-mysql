""" 
  1) consulte la información del archivo data.py
  cree un objeto que contenga las empresas y dentro 
  las sucursales que corresponden para cada empresa
"""

from data import *

def get_companies_with_branches() -> dict:
  companies_with_branches = {}
  for company in get_companies():
    companies_with_branches[company["name"]]=[]
    for branch in get_branches():
      if branch["id"] in company["branches"]:
        companies_with_branches[company["name"]].append(branch)
  print(companies_with_branches)
  return companies_with_branches
