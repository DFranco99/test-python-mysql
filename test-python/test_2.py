"""
  2) teniendo en cuenta el punto 1, cree una función que permita
  consultar las sucursales, debe hacerse el mismo procedimiento
  que se realizó en el punto 1, pero, utilizando la función creada
"""
from test_1 import *

def consult_branch() -> None:
  print(get_companies_with_branches())

consult_branch()