/* 
Teniendo en cuenta los archivos:
- softpymes_test.png
- softpymes_test.sql
Generar scripts que realicen las siguientes consultas:
*/
USE test_mysql;
/* 1. Consultar los items que pertenezcan a la compañia con ID #3 (debe utilizar INNER JOIN) */

SELECT * FROM test_mysql.items as i INNER JOIN test_mysql.companies as c on i.companyId = c.id where c.id = 3;
/* 2. Mostrar los items para los cuales su precio se encuentre en el rango 70000 a 90000*/

SELECT * FROM test_mysql.items where price BETWEEN 70000 AND 90000;
/* 3. Mostrar los items que en el nombre inicien con la letra "A" */

SELECT * FROM test_mysql.items WHERE name like "A%";
/* 4. Mostrar los items que tengan relacionado el color Rojo */

SELECT * FROM test_mysql.items as i INNER JOIN test_mysql.colors as c on i.colorId = c.id where c.id = 2;
/* 5. Se requiere asignar un precio a los items cuyo precio sea NULL, 
el precio a agregar debe ser calculado de la siguiente forma: costo del item + 10.000*/

UPDATE test_mysql.items SET price = cost + 10000 WHERE price is null;
/* 6. Incrementar el precio de los items en un 20% */

UPDATE test_mysql.items SET price = price + ((price*20)/100);
/* 7. Consultar los items que terminen en la letra "A" en el nombre, y anteponer la 
palabra "Nuevo" */

UPDATE test_mysql.items SET name = concat('Nuevo', ' ', name) WHERE name like "%A";
/* 8. Eliminar los items que pertenezcan a la compañía con ID #1 */

DELETE FROM test_mysql.items where companyId = 1;
/* 9. Eliminar los items que tengan el costo menor a 10.000 */

DELETE FROM test_mysql.items where cost < 10000;
/* 10. Cree una función que permita insertar registros en la tabla colores*/


/* 11. Eliminar todos los datos de la tabla colores */

DELETE FROM test_mysql.colors;
/* 12. Agregar un campo llamado "description" en la tabla items, que permita ser NULL, 
y que tenga un máximo de 200 caracteres */

ALTER TABLE `test_mysql`.`items` ADD COLUMN `description` VARCHAR(200) NULL DEFAULT NULL AFTER `price`;